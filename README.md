# BowTie Theme by ![bowtie](/source/images/bowtiefavicon.png) [BowTie Beej](http://bowtiebeej.com)

### Contributors
* BJ Burns

## Installation

### Install

``` bash
$ git clone https://BowTieBeej@bitbucket.org/BowTieBeej/bowtietheme.git themes/bowtie
```

**BowTie requires Hexo 2.4 and above.**

### Enable

Modify 'theme' setting in '_config.yml' to 'bowtie'.

### Update

``` bash
$ cd themes/BowTie
$ git pull
```
