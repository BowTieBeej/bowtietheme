// From Landscape Theme
$(document).ready(function(){
  // Search
    var $searchWrap = $('#search-form-wrap');
    isSearchAnim = false;
    searchAnimDuration = 200;

  var startSearchAnim = function(){
    isSearchAnim = true;
  };

  var stopSearchAnim = function(callback){
    setTimeout(function(){
      isSearchAnim = false;
      callback && callback();
    }, searchAnimDuration);
  };

  $('#nav-search-btn').on('click', function(){
    if (isSearchAnim) return;

    startSearchAnim();
    $searchWrap.addClass('on');
    stopSearchAnim(function(){
      $('.search-form-input').focus();
    });
  });

  $('.search-form-input').on('blur', function(){
    startSearchAnim();
    $searchWrap.removeClass('on');
    stopSearchAnim();
  });

  // Share
  $('body').on('click', function(){
    $('.article-share-box.on').removeClass('on');
  }).on('click', '.article-share-link', function(e){
    e.stopPropagation();

    var $this = $(this),
      url = $this.attr('data-url'),
      encodedUrl = encodeURIComponent(url),
      id = 'article-share-box-' + $this.attr('data-id'),
      offset = $this.offset();

    if ($('#' + id).length){
      var box = $('#' + id);

      if (box.hasClass('on')){
        box.removeClass('on');
        return;
      }
    } else {
      var html = [
        '<div id="' + id + '" class="article-share-box">',
          '<input class="article-share-input" value="' + url + '">',
          '<div class="article-share-links">',
            '<a href="https://twitter.com/intent/tweet?url=' + encodedUrl + '" class="article-share-twitter" target="_blank" title="Twitter"></a>',
            '<a href="https://www.facebook.com/sharer.php?u=' + encodedUrl + '" class="article-share-facebook" target="_blank" title="Facebook"></a>',
            '<a href="http://pinterest.com/pin/create/button/?url=' + encodedUrl + '" class="article-share-pinterest" target="_blank" title="Pinterest"></a>',
            '<a href="https://plus.google.com/share?url=' + encodedUrl + '" class="article-share-google" target="_blank" title="Google+"></a>',
          '</div>',
        '</div>'
      ].join('');

      var box = $(html);

      $('body').append(box);
    }

    $('.article-share-box.on').hide();

    box.css({
      top: offset.top + 25,
      left: offset.left
    }).addClass('on');
  }).on('click', '.article-share-box', function(e){
    e.stopPropagation();
  }).on('click', '.article-share-box-input', function(){
    $(this).select();
  }).on('click', '.article-share-box-link', function(e){
    e.preventDefault();
    e.stopPropagation();

    window.open(this.href, 'article-share-box-window-' + Date.now(), 'width=500,height=450');
  });

  // Caption
  $('.article-entry').each(function(i){
    $(this).find('img').each(function(){
      if ($(this).parent().hasClass('fancybox')) return;

      var alt = this.alt;

      if (alt) $(this).after('<span class="caption">' + alt + '</span>');

      $(this).wrap('<a href="' + this.src + '" title="' + alt + '" class="fancybox"></a>');
    });

    $(this).find('.fancybox').each(function(){
      $(this).attr('rel', 'article' + i);
    });
  });

  if ($.fancybox){
    $('.fancybox').fancybox();
  }

 // Mobile nav
  var $container = $('#container');
  isMobileNavAnim = false;
    mobileNavAnimDuration = 200;

  var startMobileNavAnim = function(){
    isMobileNavAnim = true;
  };

  var stopMobileNavAnim = function() {
      setTimeout(function() {
          isMobileNavAnim = false;
      }, mobileNavAnimDuration);
  };

  $('#main-nav-toggle').on('click', function(){
    if (isMobileNavAnim) return;

    startMobileNavAnim();
    $container.toggleClass('mobile-nav-on');
    stopMobileNavAnim();
  });

  $('#wrap').on('click', function(){
    if (isMobileNavAnim || !$container.hasClass('mobile-nav-on')) return;

    $container.removeClass('mobile-nav-on');
  });
});

//Function for Contact Form
function ContactForm() {
    [].slice.call(document.querySelectorAll('.input')).forEach(function(inputEl) {
        // in case the input is already filled..
        if (inputEl.value.trim() !== '') {
            jQuery(this).parent('fieldset').addClass('input--filled');
        }

        // events:
        inputEl.addEventListener('focus', onInputFocus);
        inputEl.addEventListener('blur', onInputBlur);
    });

    function onInputFocus(ev) {
        jQuery(this).parent('fieldset').addClass('input--filled');
    };

    function onInputBlur(ev) {
        if (ev.target.value.trim() === '') {
            jQuery(this).parent('fieldset').removeClass('input--filled');
        };
    };
};

// Converts Two Digit Month to String
function DateToMonth(twoDigit, abbrev) {
    var archMonth;
    if (abbrev === true){
        switch (twoDigit){
            case 1:
                archMonth = "Jan";
                break;
            case 2:
                archMonth = "Feb";
                break;
            case 3:
                archMonth = "Mar";
                break;
            case 4:
                archMonth = "Apr";
                break;
            case 5:
                archMonth = "May";
                break;
            case 6:
                archMonth = "Jun";
                break;
            case 7:
                archMonth = "Jul";
                break;
            case 8:
                archMonth = "Aug";
                break;
            case 9:
                archMonth = "Sep";
                break;
            case 10:
                archMonth = "Oct";
                break;
            case 11:
                archMonth = "Nov";
                break;
            case 12:
                archMonth = "Dec";
                break;
            default:
                alert("Not A Month in this calendar");
                archMonth = "Undefined";
                break;
        };
    } else {
        switch (twoDigit){
            case 1:
                archMonth = "January";
                break;
            case 2:
                archMonth = "February";
                break;
            case 3:
                archMonth = "March";
                break;
            case 4:
                archMonth = "April";
                break;
            case 5:
                archMonth = "May";
                break;
            case 6:
                archMonth = "June";
                break;
            case 7:
                archMonth = "July";
                break;
            case 8:
                archMonth = "August";
                break;
            case 9:
                archMonth = "Sepember";
                break;
            case 10:
                archMonth = "October";
                break;
            case 11:
                archMonth = "November";
                break;
            case 12:
                archMonth = "December";
                break;
            default:
                alert("Not A Month in this calendar");
                archMonth = "Undefined";
                break;
        };
    };
    return archMonth;
};